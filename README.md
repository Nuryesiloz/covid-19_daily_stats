# COVID-19 Daily Statistics (Canada & U.S.)

This project uses a simple Python script to extract the reported numbers of Canadian COVID-19 cases from the [2019 Novel Coronavirus data repository](https://github.com/CSSEGISandData/COVID-19) operated by the Johns Hopkins University Center for Systems Science and Engineering (JHU CSSE). The script runs daily to extract and summarize the Canadian and U.S. COVID-19 numbers. The summarized data is stored in CSV files which are linked to the following Google Charts for visualization:

- [Canadian COVID-19 Time Series](https://docs.google.com/spreadsheets/d/e/2PACX-1vTDdhLAdP_Ry6CzicGCgzCA-5IOKjk1oyLKJy511vPJkAZADrLLiVuYs1IDeF3JCQghg2vV-Ozsw9EY/pubchart?oid=82403924&format=interactive) / [Image format](https://docs.google.com/spreadsheets/d/e/2PACX-1vTDdhLAdP_Ry6CzicGCgzCA-5IOKjk1oyLKJy511vPJkAZADrLLiVuYs1IDeF3JCQghg2vV-Ozsw9EY/pubchart?oid=82403924&format=image)
- [U.S. COVID-19 Time Series](https://docs.google.com/spreadsheets/d/e/2PACX-1vQaAKficm9JD2Sf2h_mwGlNr7IVGjWQDOv_5hS_K9xcsAimWxE_LZOhczmLJREtxopMhERj2Xob8Qu7/pubchart?oid=82403924&format=interactive) / [Image format](https://docs.google.com/spreadsheets/d/e/2PACX-1vQaAKficm9JD2Sf2h_mwGlNr7IVGjWQDOv_5hS_K9xcsAimWxE_LZOhczmLJREtxopMhERj2Xob8Qu7/pubchart?oid=82403924&format=image)

As the JHU repository does not track all statistics to the provincial level, I recently created a second script which extracts the Ontario [COVID-19 statistics](https://data.ontario.ca/dataset/status-of-covid-19-cases-in-ontario/resource/ed270bb8-340b-41f9-a7c6-e8ef587e6d11) from the Government of Ontario [Data Catalogue](https://data.ontario.ca). This script normalizes the data into a format comparable to the JHU statistics which is then linked to the following Google Chart for visualization:

- [Ontario COVID-19 Time Series](https://docs.google.com/spreadsheets/d/e/2PACX-1vSSQqoGLESn8MCwizUz6OFuCjKlI6_v548awt1OMe-l569zZSq7UbNJ2LPbbkzs1ocIZvcHLW9o1IRc/pubchart?oid=82403924&format=interactive) / [Image format](https://docs.google.com/spreadsheets/d/e/2PACX-1vSSQqoGLESn8MCwizUz6OFuCjKlI6_v548awt1OMe-l569zZSq7UbNJ2LPbbkzs1ocIZvcHLW9o1IRc/pubchart?oid=82403924&format=image)

I created this project for my personal use but left it open to the public for anyone interested in keeping tabs on the North American COVID-19 numbers.
