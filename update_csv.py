#!/usr/bin/python
import csv,urllib
from datetime import datetime,timedelta

yesterday = datetime.now() - timedelta(1)
datestr = yesterday.strftime('%b-%d')
url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/" + yesterday.strftime("%m-%d-%Y") + ".csv"

webpage = urllib.urlopen(url)
datareader = csv.DictReader(webpage)
data = [row for row in datareader]

if data:
    for country in ["Canada","US"]:
        confirmed = 0
        deaths = 0
        recovered = 0

        filename = "./Covid-19_Data_" + country.upper() + ".csv"

        for row in data:
            if row['Country_Region'] == country:
                confirmed = confirmed + int(row['Confirmed'])
                deaths = deaths + int(row['Deaths'])
                recovered = recovered + int(row['Recovered'])

        current = confirmed - (deaths + recovered)
        append_row = [datestr,str(confirmed),str(deaths),str(recovered),str(current)]
        with open(filename,'a') as fd:
            writer = csv.writer(fd)
            writer.writerow(append_row)
