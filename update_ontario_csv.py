#!/usr/bin/python
import csv,urllib
from datetime import datetime,timedelta

url = "https://data.ontario.ca/dataset/f4f86e54-872d-43f8-8a86-3892fd3cb5e6/resource/ed270bb8-340b-41f9-a7c6-e8ef587e6d11/download/covidtesting.csv"
webpage = urllib.urlopen(url)
datareader = csv.DictReader(webpage)
data = [row for row in datareader]

if data:
    filename = "./Covid-19_Data_ONTARIO.csv"
    resultFile = open(filename,'w')
    writer = csv.writer(resultFile)
    writer.writerow(["DATE","Confirmed","Deaths","Recovered","Active"])

    for row in data:
        append_row = [row['Reported Date'],row['Total Cases'],row['Deaths'],row['Resolved'],row['Confirmed Positive']]
        writer.writerow(append_row)
